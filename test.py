#!/usr/bin/python
"""Generate an html file.
"""

import argparse
import requests

if __name__ == "__main__":
    description = __doc__
    argParser = argparse.ArgumentParser(description=description)
    argParser.add_argument("--build-id",
                           help=("build id"))
    argParser.add_argument("--job-name",
                           help=("job name"))
    argParser.add_argument("filename",
                           nargs="?",
                           default="result.html",
                           help=("filename to store the result document to"))
    args = argParser.parse_args()

    url = "http://bop:8080/job/{jobName}/{buildId}/consoleText".format(
        jobName=args.job_name,
        buildId=args.build_id
    )
    stream = requests.get(url)
    lines = [l for l in stream.iter_lines()]

    with open(args.filename, 'w') as f:
        print >> f, ("<html>"
                     "<head></head>"
                        "<body>"
                            "<h1>Hello world!</h1>"
                            "How are you doing today?\n"
                            "consoleText has {lines} lines\n"
                            "first is:\n"
                            "{first}\n"
                            "last is:\n"
                            "{last}\n"
                        "</body>"
                     "</html>".format(lines=len(lines),
                                      first=lines[0],
                                      last=lines[-1]))
